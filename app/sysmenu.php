<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sysmenu extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'sysmenus';
     protected $fillable = [
          'id', 'namamenu', 'sysmenu_id', 'tgllhr', 'linkmenu'];

    public function categories()
    {
        return $this->hasMany(sysmenu::class);
    }

    public function childrenCategories()
    {
        return $this->hasMany(sysmenu::class)->with('categories');
    }

}
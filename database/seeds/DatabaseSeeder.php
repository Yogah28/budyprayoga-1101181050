<?php

use App\sysuser;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        sysuser::insert([
            'uname' => 'budpy',
            'namalengkap' => 'Budy Prayoga',
            'email' => 'budyprayoga@gmail.com',
            'upass' => sha1('yoga1998')
        ]);
    }
}

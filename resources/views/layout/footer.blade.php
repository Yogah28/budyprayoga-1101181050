<!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
      Bersama Kuli Membangun Negri
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2021 <a href="https://www.instagram.com/yogah28">BDPYz</a>.</strong> All rights reserved.
  </footer>